import cv2
import numpy as np 
from datetime import datetime

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

#FACE TRACKER
POSITIONS = []

#POSITION MARGIN
MARGIN = 50

cap = cv2.VideoCapture(0)
initial = datetime.now()
print("START TIME: ",initial)

font = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1
fontColor = (255,255,255)
lineType = 2


while True:
	
	INFORMATION = ""
	ret, img = cap.read()
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	faces = face_cascade.detectMultiScale(gray, 1.3, 15)
	
	#IF FACE IS FOUND
	if len(faces) != 0:
		for (x,y,w,h) in faces:
			bottomLeftCornerOfText = (int(x+(w/2)),int(y-15))
			cv2.putText(img,'FACE', 
	    bottomLeftCornerOfText, 
	    font, 
	    fontScale,
	    fontColor,
	    lineType)
			#current properties
			#ALWAYS [INNER, OUTER]

			myleft_boundaries = [x+MARGIN, x-MARGIN]
			myright_boundaries = [x+w-MARGIN, x+w+MARGIN]
			
			mytop_boundaries = [y-MARGIN, y+MARGIN]
			mybottom_boundaries = [y+h-MARGIN, y+h+MARGIN]

			#CHECKING IF 


			POSITIONS.append([x,y])
			cv2.rectangle(img, (x,y), (x+w, y+h), (255,0,0), 2)
			roi_gray = gray[y:y+h, x:x+w]
			roi_color = img[y:y+h, x:x+w]
			eyes = eye_cascade.detectMultiScale(roi_gray)
			INFORMATION += "FOUND " + str(len(faces)) + " FACES "
			
			#IF EYES ARE FOUND
			if len(eyes) != 0:	
				INFORMATION += " EYES " 	
				for (ex, ey, ew, eh) in eyes:
					cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)

			else:
				INFORMATION += " NO EYES "
	
	#IF FACE IS NOT FOUND
	else:
		INFORMATION += " NO FACE,"

	INFORMATION += str(datetime.now() - initial)
	
	cv2.imshow('img', img)
	k = cv2.waitKey(30) & 0xff
	if k == 26:
		break

	#PRINT STATEMENTS
	print(INFORMATION)
	print(POSITIONS)
        
cap.release().read()
cv2.destroy() 
